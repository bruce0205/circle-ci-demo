[![CircleCI](https://circleci.com/bb/ab24627375/circle-ci-demo.svg?style=svg)](https://circleci.com/bb/ab24627375/circle-ci-demo)
[![GitHub license](https://img.shields.io/badge/license-MIT-green.svg)](https://bitbucket.org/ab24627375/circle-ci-demo/src/38a4bd0cddc4ed461cc290d3b6c9717edefd1257/LICENSE?at=master&fileviewer=file-view-default)
## Getting Started 
### Clone Project 
 you can create a new project based on circle-ci-demo by doing the following:
modify
```bash
$ git clone https://ab24627375@bitbucket.org/ab24627375/circle-ci-demo.git
$ cd circle-ci-demo
```

### Installation
When that's done, install the project dependencies.You can use npm or yarn(recommended) for dependency management。

```bash
$ npm install
```

### Running the Project

After completing the [installation](#installation) step, you're ready to start the project!

| script | Description |
| ------| ------ |
| start | Serves your app at localhost:3000 |
| test | Runs unit tests with mocha  |

## Screenshot

![img](/screenshot/img-1.png)
